package rps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RpsNewProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(RpsNewProjectApplication.class, args);
    }

}
