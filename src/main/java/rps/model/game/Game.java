package rps.model.game;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import rps.model.utils.Selection;
import rps.tokens.Token;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Game")
@NoArgsConstructor
public class Game {

    @Id
    private String id;
    private Selection move;
    private Selection opponentMove;
    private State state;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn
    Token owner;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn
    Token joiner;


    public Game(String id, Token owner) {
        this.id = id;
        this.owner = owner;
        this.state = State.OPEN;
    }


    public enum State {
        OPEN(0), ACTIVE(1), WIN(2), LOSE(3), DRAW(4);
        private int value;

        State(int value) {
            this.value = value;
        }
    }


}
