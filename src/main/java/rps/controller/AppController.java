package rps.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


import rps.model.game.Game;
import rps.model.utils.Selection;
import rps.model.utils.AppUtils;
import rps.model.utils.GameDTO;
import rps.services.GameService;

import rps.tokens.Token;
import rps.tokens.TokenService;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@AllArgsConstructor
@CrossOrigin
public class AppController {

    private final GameService gameService;

    TokenService tokenService;


    @GetMapping(value = "/auth/token")
    public String getToken() {
        return tokenService.creatToken().getId();

    }


    @GetMapping(value = "/games/start")
    public GameDTO startGame(@RequestHeader("token") String tokenId) {
        Token token = tokenService.getTokenById(tokenId);
        return toGameDTO(gameService.startNewGame(token));

    }

    private GameDTO toGameDTO(Game game) {
        return new GameDTO(game.getId(),
                game.getOwner().getName(),
                game.getMove(),
                game.getState(),
                game.getJoiner() != null ? game.getJoiner().getName() : "",
                game.getOpponentMove());
    }


    @GetMapping(value = "/games/join/{gameId}")
    public GameDTO joinGame(@PathVariable("gameId") String gameId,
                            @RequestHeader("token") String tokenId) {
        Token token = tokenService.getTokenById(tokenId);
        return toGameDTO(gameService.joinGame(gameId, token));
    }


    @GetMapping(value = "/games/move/{sign}")
    public GameDTO makeMove(@PathVariable("sign") Selection move,
                            @RequestHeader("token") String tokenId) {
        return toGameDTO(gameService.makeMove(move, tokenId));
    }


    @GetMapping(value = "/games/status")
    public GameDTO getState(@RequestHeader("token") String tokenId) {
        return toGameDTO(gameService.getState(tokenId));
    }


    @GetMapping(value = "/games")
    public List<GameDTO> getAllGames(@RequestHeader(value = "token", required = true) String tokenId) {
        return gameService.getAllGames(tokenId).map(game -> toGameDTO(game)).collect(Collectors.toList());
    }


}
